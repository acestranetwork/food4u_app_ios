angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal,$interval,$timeout,$state,service,$cordovaToast,$rootScope,mylaipush,$ionicPlatform) {

  // app_user=JSON.parse(localStorage.getItem("food_app_user"));
  if(app_user != null)
  {
    var obj = {'token':token,'user_id':app_user.id};
    service.post(url_new+'user_info',obj).then(function(result){
      if(status == 'success')
      {
       localStorage.setItem('food_app_user',JSON.stringify(result.data));
       app_user = result.data;
     }
   });
  }
  count_cart = function(){
    data=JSON.parse(localStorage.getItem("food_cart"));
    if(data && data.products.length > 0)
    {
      $scope.cart_count = data.products.length;
    }
    else
    {
      $scope.cart_count = '';
    }
  }
  count_cart();


  $rootScope.check = function()
  {
    if(app_user && app_user.role != '')
    {
      $scope.role = app_user.role;
      $scope.user = app_user;
    }

  }
  // $rootScope.check_track = function(status){
  //   if(app_user != {} && status==true && app_user.role == "DeliveryBoy")
  //   {
  //     interval = $interval(track, 30000);
  //   }
  //   else
  //   {
  //     $interval.cancel(interval);
  //   }
  // }

  $rootScope.check();

  $scope.logout = function() {

    data = {};
    data.token = token;
    data.log_id = app_user.log_id;

    service.post(url_new+'logout' , data).then(function(result) {
      console.log(result)
      if(result.status == "success")
      {
        $interval.cancel(interval);

        localStorage.removeItem('food_app_user');
        localStorage.removeItem('device_token')
        app_user = {};
        $scope.user = "";
        $scope.status = false;
        $scope.role = "";
        $state.go('tab.restaurants');
      }
    })
  };
  get_notification = function()
  {
   app_id='WO_CFoA_O1tb23feBFTsK8u-r';

   model=ionic.Platform.device();

   push = PushNotification.init({"android":{ "senderID": "8060007986"}});

   push.on('registration', function(data) {
    device_token=data.registrationId;
    get_token=JSON.parse(localStorage.getItem('device_token'));
    if(get_token != device_token || !get_token || get_token=='' || get_token==null){
      localStorage.setItem('device_token',JSON.stringify(device_token));
      var obj={'app_id':app_id,'device_token':device_token,'model':model.model,'platform':model.platform,'status':true}

      mylaipush.notification(obj);
    }
    notification_data = {};
    notification_data.token = token;
    notification_data.user_id = app_user.id;
    notification_data.device_token = device_token;

    service.post(url_new+'device_register' , notification_data).then(function(result) {
    })

  });

 }

})
.controller('LoginCtrl', function($scope,$stateParams,service,$state,$cordovaToast,$rootScope,$ionicPlatform) {
 $scope.doLogin = function(data) {
  model=ionic.Platform.device();
  if(data){
    data.token = token;
    data.device = model.model;
    // $ionicLoading.show({template: '<ion-spinner icon="bubbles" class="spinner-calm"></ion-spinner>'})

    service.post(url_new+'userlogin', data).then(function(result) {
      if(result.status == 'success')
      {
       console.log(result)
       // $ionicLoading.hide();

       app_user = result.data;
       $scope.role = result.data.role;
       localStorage.setItem('food_app_user',JSON.stringify(app_user));
       $scope.role = result.data.role;

          // get_notification();
          $state.go('tab.restaurants');
          $rootScope.check();
        }
        else
        {
          $cordovaToast.showShortBottom(result.message);

        }
      }, function(error) {
        if (error == 500 || error == 404 || error == 0) {
          $cordovaToast.showShortBottom('Network Failed' + error);
        }
      });
  }
  else
  {
    $cordovaToast.showShortBottom('fill all fields');
  }
};
})
.controller('RegisterCtrl', function($scope,$state,service,$cordovaToast,$cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice,
  $ionicPopup, $ionicActionSheet) {
 $scope.register = {};

 $scope.reg = function (user) {
  if(user.password == user.confirm_password)
  {
    user.token = token;

    // $ionicLoading.show({
    //   template: '<ion-spinner icon="ios" class="spinner-assertive"></ion-spinner>'
    // })
    service.post(url_new+'usersignup', user).then(function(result) {
      console.log(result);
      if(result.status =='success')
      {
       $scope.register = {};
       $state.go('login');
     }
   })
      // if($scope.model_image){

      //   var targetPath = $scope.pathForImage($scope.model_image);
      //   user.image = $scope.model_image;
      //   var options = {
      //     fileKey: "image",
      //     fileName: $scope.model_image,
      //     chunkedMode: false,
      //     mimeType: "multipart/form-data",
      //     params: user
      //   };

      //   $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
      //     $ionicLoading.hide();
      //     data = JSON.parse(result.response);
      //     if(data.status == 'success'){
      //      $cordovaToast.showLongCenter(data.message);
      //      $scope.reg_data = {};
      //      $state.go('app.login');
      //    }else{
      //     $cordovaToast.showLongCenter(data.message);
      //   }
      // }, function(error) {
      //   $ionicLoading.hide();
      //   $cordovaToast.showShortBottom("error"+JSON.stringify(error));

      // });
      // }else{
      //   service.post(url , user).then(function(result) {
      //     $ionicLoading.hide();
      //     $cordovaToast.showShortBottom(result.message);
      //     $state.go('app.login');
      //   }, function(error) {
      //     $cordovaToast.showShortBottom("error"+JSON.stringify(error));
      //   });
      // }
    }
    else
    {

      $cordovaToast.showLongCenter('password mismatched');
    }
  }
})

.controller('RestaurantsSearchCtrl', function($scope,Restaurants,$cordovaToast,$state,$cordovaGeolocation) {

	$scope.location = {};
	$scope.autocompleteOptions = {};

  $scope.$on('gmPlacesAutocomplete::placeChanged', function(){
    console.log($scope.location);
  });

  $scope.search_location = function(location){

   if(location.getPlace())
   {
     coords =  location.getPlace();
     cords  = {};
     cords.lat = coords.geometry.location.lat();
     cords.lng = coords.geometry.location.lng();
     localStorage.setItem('location',JSON.stringify(cords));
     $state.go('tab.restaurants',{cords:cords});
   }
   else
   {
          // $cordovaToast.showShortBottom('please search your location');

          alert('please search your location');
        }
      }
      $scope.current_location = function(){

        // navigator.geolocation.getCurrentPosition(function(pos){
        //
        //   cords  = {};
        //   cords.lat = pos.coords.latitude;
        //   cords.lng = pos.coords.longitude;
        //   localStorage.setItem('location',JSON.stringify(cords));
        //
        //
        //   $state.go('tab.restaurants',{cords:cords});
        // })
        var posOptions = { timeout: 50000, enableHighAccuracy: true };
        $cordovaGeolocation.getCurrentPosition(posOptions).then(function(pos) {
                  cords  = {};
                  cords.lat = pos.coords.latitude;
                  cords.lng = pos.coords.longitude;
                  localStorage.setItem('location',JSON.stringify(cords));


                  $state.go('tab.restaurants',{cords:cords});

        }, function(err) {
          console.log(err);

        })
      }
    })
.controller('RestaurantsCtrl', function($scope, Restaurants,service,$stateParams, $ionicActionSheet, $timeout,$ionicModal,Map) {

  if($stateParams.cords != null)
  {
    coords = $stateParams.cords;
  }
  else
  {
    cords = JSON.parse(localStorage.getItem('location'));
  }
  if(cords)
  {
   var geocoder = new google.maps.Geocoder;

   geocoder.geocode({'location': cords}, function(results, status) {
    if (status === 'OK') {
      results.forEach(function(value,key){
        if(value.types[0] == 'street_address')
        {
         $scope.address = value.formatted_address;
       }
     })
    }
  })
 }
 $scope.img_url = img_url;
 data = {};
 data.token = token;
 data.lat = cords.lat;
 data.lng = cords.lng;
 data.filter = '';

hotel = function(data)
{
  service.post(url_new+'nearby_hotels', data).then(function(result) {
   console.log(result)
   $scope.restaurants = result.data;
 })
}

hotel(data);

food_type_filter = function(type)
{
  data.type = type;
  service.post(url_new+'food_type_filter', data).then(function(result) {
   console.log(result)
   $scope.restaurants = result.data;
     hideSheet();
 })
}

 $scope.remove = function(restaurant) {
  Restaurants.remove(restaurant);
};
$scope.data = {
  grid: true
};

var hideSheet = '';

$scope.show = function() {

 hideSheet = $ionicActionSheet.show({
   buttons: [
   { text: 'Vegetarian' },
   { text: 'Non-vegetarian' }
   ],
   titleText: 'Filter Your Restaurant',
   cancelText: 'Cancel',
   cancel: function() {

        },
        buttonClicked: function(index) {
          if(index == 0)
          {
            type = 'veg';
          }
          else if(index == 1)
          {
            type = 'non_veg';
          }

        food_type_filter(type);
       }
     });

   // $timeout(function() {
   //   hideSheet();
   // }, 2000);

 };
 $ionicModal.fromTemplateUrl('templates/filter.html', {
  scope: $scope,
  animation: 'slide-in-up'
}).then(function(modal) {
  $scope.modal = modal;
});
$scope.show_filter = function()
{
  service.post(url_new+'categorylist',{token:token}).then(function(res){
    console.log(res);
    if(res.status == 'success')
    {
      $scope.categories = res.data;
      $scope.modal.show();
    }
  })
}
$scope.close_filter = function(){
  $scope.data.filter = {};
      $scope.modal.hide();
}
$scope.apply_filter = function(data){
  console.log(data.filter);
  data.token = token;
 data.lat = cords.lat;
 data.lng = cords.lng;
 console.log(data);
// hotel(data);
 service.post(url_new+'filter_list',data).then(function(res){
    console.log(res);
    if(res.status == 'success')
    {
      $scope.restaurants = res.data;
      $scope.data.filter = {};
      $scope.modal.hide();
    }
  })
}
})

.controller('RestaurantDetailCtrl', function ($scope,service,$ionicPopup, $rootScope, $stateParams,$cordovaToast, $ionicScrollDelegate, Restaurants, $ionicPopover, $location, $window, $ionicSuperActionSheet) {

	restaurantId = $stateParams.restaurantId;
	data = {};
	data.token = token;
	data.branch_id = restaurantId;
	$scope.img_url = img_url;
	$scope.curency = curency;

  service.post(url_new+'productlist', data).then(function(result) {
   console.log(result)
   $scope.products = result.data.products;
   $scope.restaurant = result.data.restaurant;
   $scope.menus = result.data.menu;
console.log(result.data)
 })

 //  service.post(url_new+'hotel_menu', data).then(function(result) {
 //   console.log(result)
 //   $scope.products = result.data.products;
 //   $scope.restaurant = result.data.restaurant;
 // })
  count_cart();

  $scope.openFavISAS = function () {
    $rootScope.$broadcast('open', 'sof2');
  };
	// .fromTemplateUrl() method
	$ionicPopover.fromTemplateUrl('my-popover.html', {
		scope: $scope
	}).then(function (popover) {
		$scope.popover = popover;
	});
	$scope.scrollTo = function (target) {
		$location.hash(target); //set the location hash
		var handle = $ionicScrollDelegate.$getByHandle('myPageDelegate');
		handle.anchorScroll(true); // 'true' for animation
	};
	$scope.openPopover = function ($event) {
		$scope.popover.show($event);
	};
	$scope.closePopover = function () {
		$scope.popover.hide();
	};
	//Cleanup the popover when we're done with it!
	$scope.$on('$destroy', function () {
		$scope.popover.remove();
	});
	// Execute action on hidden popover
	$scope.$on('popover.hidden', function () {
		// Execute action
	});
	// Execute action on remove popover
	$scope.$on('popover.removed', function () {
		// Execute action
	});
	$scope.changeHeader = function (id) {
		var el = document.getElementById(id)
   , windowHeight = $window.innerHeight
   , scrollPosition = $ionicScrollDelegate.getScrollPosition().top - windowHeight / 5;
   var alpha = scrollPosition / windowHeight * 7;
   el.style.backgroundColor = "rgba(241,85,34," + alpha + ")";
   el.style.borderBottomColor = "rgba(241,85,34," + alpha + ")";
 }
 angular.element(document).ready(function () {
  document.getElementById('myFunction').onscroll = function () {
   $scope.changeHeader('ben-header');
 };
});


 addTO_cart = function(cart_product,cart_data)
 {
   if(!cart_product){

    cart=[];
    data = {};
    data.id = cart_data.id;
    data.quantity = 1;
    data.price = cart_data.price;
    cart.push(data);
    // $cordovaToast.showLongBottom('Product added successfully to your cart');
  }
  else
  {
    cart = cart_product.products;

    angular.forEach(cart,function(value,key){
      if(value.id == cart_data.id)
      {
        check.push(cart_data.id);
      }
    })
    if(check.length == 0)
    {
      data = {};
      data.id = cart_data.id;
      data.quantity = 1;
      data.price = cart_data.price;
      cart.push(data);
      // $cordovaToast.showShortBottom('Product added successfully to your cart');
    }
    else
    {
      // $cordovaToast.showShortBottom("already in cart");
    }

  }
  cart_value = {};

  cart_value.restaurantId = restaurantId;
  cart_value.products = cart;

  localStorage.setItem('food_cart',JSON.stringify(cart_value));
  count_cart();

}

$scope.add_cart = function(cart_data){
  console.log(cart_data)
  cart = [];
  check = [];
  cart_product=JSON.parse(localStorage.getItem("food_cart"));
  if(!cart_product)
  {
    addTO_cart(cart_product,cart_data);
  }
  else
  {
    if(restaurantId == cart_product.restaurantId)
    {
      addTO_cart(cart_product,cart_data);
    }
    else
    {
      $ionicPopup.show({
       title : 'Items already in cart',
       template : 'Your cart contains items from other restaurant. Are you sure want to reset cart and add this item?',
       buttons : [{
        text : 'Cancel',
        type : 'button-assertive button-outline',
      }, {
        text : 'Ok',
        type : 'button-assertive',
        onTap : function() {
          localStorage.removeItem('food_cart');
          addTO_cart('',cart_data);
        }
      }]
    });
    }
  }
}
})

.controller('OrderCtrl', function($scope, Restaurants, $ionicSlideBoxDelegate) {
  var restaurants = Restaurants.all();

  restaurants.then(function(response){
    $scope.restaurants = response;

    $ionicSlideBoxDelegate.update();
  //assign data here to your $scope object
},function(error){
  console.log(error);
})


})

.controller('NotificationCtrl', function($scope, Restaurants) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  var restaurants = Restaurants.all();

  restaurants.then(function(response){
    $scope.restaurants = response;

  //assign data here to your $scope object
},function(error){
  console.log(error);
})
//  $scope.chats = Chats.all();
//  $scope.remove = function(chat) {
//    Chats.remove(chat);
//  };
})

//.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
//  $scope.chat = Chats.get($stateParams.chatId);
//})

.controller('CartCtrl', function($scope,$state,Restaurants,$ionicSlideBoxDelegate,service) {

 $scope.total;
 $scope.appuser = app_user;
 console.log($scope.appuser)

 $scope.img_url = img_url;
 $scope.curency = curency;

 cart_list = function(){

  $scope.total = 0;
  cart=JSON.parse(localStorage.getItem("food_cart"));
  if(cart)
  {
    cart_data = cart.products;
    cart_products = [];
    if(cart_data && cart_data.length > 0)
    {
     cart_data.forEach(function(value){
      cart_products.push(value.id);
    })

     var obj = {'token':token,'products_id':cart_products,'branch_id':cart.restaurantId};
     service.post(url_new+'cart_products' , obj).then(function(result) {

       if(result.status == 'success')
       {
        angular.forEach(result.data.products, function(value,key){

          angular.forEach(cart_data,function(values,key){

            if(value.id == values.id){
             value.quantity = values.quantity;
             $scope.total += (value.quantity*parseInt(value.price));
           }

         })
          $scope.productlist = result.data.products;
          $scope.restaurant = result.data.hotel;
        })
      }
      else
      {
        $cordovaToast.showShortBottom(result.text);
      }

    }, function(error) {
      if (error == 500 || error == 404 || error == 0) {

        // $cordovaToast.showShortBottom(error);
      }
    });
   }
 }
 else
 {
  $scope.restaurant = '';
  $scope.productlist = '';
}

}
cart_list();
$scope.sub = function(i) {
  if(i.quantity  >> 1)
  {
    i.quantity--;
    cart_qty(i.id,i.quantity);
  }
}
$scope.add = function(i) {
  // if(i.quantity < i.stock)
  // {
    i.quantity++;
  // }
  // else
  // {
  //   $cordovaToast.showShortBottom('required quantity higher than available stock');
  // }
  // if(i.quantity <= i.stock)
  // {
    cart_qty(i.id,i.quantity);
  // }

}
$scope.delete = function(list) {

 angular.forEach(cart_data, function(value,key){

  if(value.id == list.id){
    cart_data.splice(key,1);
    $scope.total -= (value.quantity*value.price);
  }
})
 if(cart_data.length > 0)
 {
  cart.products = cart_data;
  localStorage.setItem("food_cart",JSON.stringify(cart));
}
else
{
  localStorage.removeItem("food_cart");

}

// $cordovaToast.showShortBottom('Product deleted successfully from your cart');
cart_list();
count_cart();
}
cart_qty = function(id,qty){
  $scope.total = 0;
  cart_data.forEach(function(value){
    if(value.id == id)
    {
      value.quantity = qty;
    }
    $scope.total += (value.quantity*value.price)
  })
  cart.products = cart_data;
  localStorage.setItem("food_cart",JSON.stringify(cart));
}
$scope.checkout = function(){
  console.log(app_user)
  if(app_user != null)
  {
    data = {};
    data.total = $scope.total;
    data.cart_data = cart_data;
    $state.go('tab.checkout',{data:data});
  }
  else
  {
    $cordovaToast.showShortBottom('login to checkout');
    $state.go('login');
  }
}

})


.controller('CheckoutCtrl', function($scope,service,$stateParams,$state) {
  console.log($stateParams.data);

  product_data = $stateParams.data;
  $scope.user={};

  $scope.shipping_address={};
  $scope.shipping_address.userName="arun";
  $scope.shipping_address.address= "2/2 venkatesa Agraharam Street";
  $scope.shipping_address.area= "Mylapore";
  $scope.shipping_address.city="Chennai";
  $scope.shipping_address.pincode="600004";
  $scope.shipping_address.mobile="123123213";

  $scope.address_id = {};
  $scope.select_address = {};
  $scope.new_address = false;
  $scope.deliver_address = {};

  service.post(url_new+'address_list', {'token':token,'user_id':app_user.id}).then(function(result) {
$scope.shipping_address = result.data;
   console.log($scope.shipping_address)
 })
$scope.add_address = function(data)
{
  console.log(data)
  data.user_id = app_user.id;
  data.token = token;
    service.post(url_new+'add_address', data).then(function(result) {
   console.log(result)
 })
}

$scope.payment = function(data)
{  localStorage.setItem("my_address",JSON.stringify(data));
  data.total = product_data.total;
  data.products = product_data.cart_data;
  console.log(data);
  $state.go('tab.payment',{data:data});
 //  data.user_id = app_user.id;
 //  data.token = token;
 //    service.post(url_new+'add_address', data).then(function(result) {
 //   console.log(result)
 // })
}

$scope.nochange_address = function(id){
  console.log(id);
  $scope.old_address = true;
  $scope.shipping_address=JSON.parse(localStorage.getItem("my_address"));
  console.log($scope.shipping_address);
  $scope.new_address = false;
  $scope.address_id = id;
}

$scope.change_address = function(){
    $scope.old_address = false;
  $scope.new_address = true;
  $scope.address_id = 'new';
}
})

.controller('PaymentCtrl', function($scope,service,$stateParams,$state) {
  $scope.shipping_address=JSON.parse(localStorage.getItem("my_address"));
  console.log($scope.shipping_address);
  console.log($stateParams.data);
  order_data = $stateParams.data;
$scope.pay = function(type){
 order_data.type = type;
  service.post(url_new+'add_address', data).then(function(result) {
 //   console.log(result)
 })
 $state.go('tab.restaurants');
}

})


.controller('ConfirmationCtrl', function($scope) {
 $scope.footerExpand = function() {
  console.log('Footer expanded');
};
$scope.footerCollapse = function() {
  console.log('Footer collapsed');
};
})
.controller('AccountCtrl', function($scope,service,$cordovaToast,$cordovaSocialSharing) {
 $scope.appUser = app_user;
 $scope.share = function(){
  $cordovaSocialSharing
    .share("message", "subject", "file", "link") // Share via native share sheet
    .then(function(result) {
      // Success!
    }, function(err) {
      // An error occured. Show a message to the user
    });
 }
})


.controller('SettingsCtrl', function($scope) {

 $scope.appUser = app_user;

  $scope.settings = {
    enableFriends: true
  };
})
.controller('ProfileCtrl', function($scope,service,$state,$stateParams) {

 $scope.appUser = app_user;

 $scope.update = function(app_user_data) {
  console.log(app_user_data)
  app_user_data.token = token;

  // $ionicLoading.show({
  //   template: '<ion-spinner icon="ios" class="spinner-assertive"></ion-spinner>'
  // })

  if($scope.model_image){

    var targetPath = $scope.pathForImage($scope.model_image);
    app_user_data.image = $scope.model_image;
    var options = {
      fileKey: "image",
      fileName: $scope.model_image,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params: app_user_data
    };

    $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
      // $ionicLoading.hide();
      data = JSON.parse(result.data);
      app_user = data;
      localStorage.setItem('food_app_user',JSON.stringify(app_user));
      $cordovaToast.showShortBottom('Profile updated successfully');
      $state.go('app.home_page');
    }, function(error) {
      // $ionicLoading.hide();
      $cordovaToast.showShortBottom("error"+JSON.stringify(error));

    });
  }else{
    service.post(url_new+'usereditprfl' , app_user_data).then(function(result) {
      console.log(result)
      // $ionicLoading.hide();
      app_user = result.data;
      localStorage.setItem('food_app_user',JSON.stringify(result.data));
      // $cordovaToast.showShortBottom('Profile updated successfully');
      $state.go('tab.account');
    }, function(error) {
      // $ionicLoading.hide();
      // $cordovaToast.showShortBottom("error"+JSON.stringify(error));
    });
  }
}

});
