angular.module('starter.services', [])

 .factory('service', function($http, $q, $window) {
  return {
    get: function(url) {
      var deferred = $q.defer();
      $http.get(url)
        .success(function(data) {
          deferred.resolve(data);
        }).error(function(msg, code) {
          deferred.reject(code);
        });
      return deferred.promise;
    },
    post: function(url, obj) {
      var deferred = $q.defer();
      $http.post(url, obj)
        .success(function(data) {
          deferred.resolve(data);
        }).error(function(msg, code) {
          deferred.reject(code);
        });
      return deferred.promise;
    }
  };
})
.factory('Restaurants', function($http,$q) {
  // Might use a resource here that returns a JSON array

 var restaurants = [];
	
  return {
    all: function() {
		 var deffered = $q.defer();
      $http({
        method: 'GET',
        url: '../json/restaurants.json'
      }).success(function (data, status, headers, config) {
		 restaurants = data;
  deffered.resolve( restaurants);
        console.log( restaurants);
		  
		   }).error(function (data, status, headers, config) {
          deffered.reject(status);
        });
		    return deffered.promise;
    },
    remove: function(restaurant) {
      restaurants.splice(restaurants.indexOf(restaurant), 1);
    },
    get: function(restaurantId) {
      for (var i = 0; i < restaurants.length; i++) {
        if (restaurants[i].id === parseInt(restaurantId)) {
          return restaurants[i];
        }
      }
      return null;
    }
	  
  };
})
.factory('mylaipush', function($http) {
  return {
    notification: function(obj) {
      $http.post('https://push.mylaporetoday.in/device_register', obj).then(function(result) {
        if (result.status == "success") {
        } else if (result.status == "error") {
          console.log(result.message);
        }
      }, function(error) {
        if (error == 500 || error == 404 || error == 0) {
          console.log("Push Api" + error);
        }
      });
    },
  };
})

.factory('Map',function($http,$q){
  var marker;
  var line;
  var map;
  return{
    init:function(lat,lon) {

      map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: new google.maps.LatLng(lat,lon)
      });
      places = new google.maps.places.PlacesService(map);
    },
    Marker:function(res) {
     marker = new google.maps.Marker({
      map: map,
      position: {lat:res.lat,lng:res.lng},
      animation: null,
            // draggable: true,
            title: res.customer_name
          });
          // map.setCenter(res);
        },
        addMarker:function(res,icon) {
         if (line != null) {
          line.setMap(null);
        }
        if (marker != null) {
          marker.setMap(null);
        }
          var lineSymbol = {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 6,
            strokeOpacity: 0.8,
            strokeWeight: 1,
            fillOpacity: 0.8,
            fillColor:'#095FDC',
            strokeColor: '#095FDC',
          };
        marker = new google.maps.Marker({
          map: map,
          position: {lat:res.lat,lng:res.lng},
          icon: lineSymbol,
          animation: null,
            // draggable: true,
            title: res.customer_name
          });
          // map.setCenter(res);
        },
        autosearch:function(input) {
          autocomplete = new google.maps.places.Autocomplete(input);
          autocomplete.bindTo('bounds', map);
          return autocomplete;
        },
        search:function(str) {
          var d = $q.defer();
          places.textSearch({query: str}, function(results, status) {
            if (status == 'OK') {
              d.resolve(results[0]);
            }
            else d.reject(status);
          });
          return d.promise;
        },
        direction:function(waypts,start,end) {
          var directionsService = new google.maps.DirectionsService;
          var directionsDisplay = new google.maps.DirectionsRenderer;
          
          directionsDisplay.setMap(map);

          directionsService.route({
            origin: start,
            destination: end,
            waypoints: waypts,
            optimizeWaypoints: false,
            travelMode: 'DRIVING'
          }, function(response, status) {
            if (status === 'OK') {
              console.log(response);
              directionsDisplay.setDirections(response);

            } else {
              alert('Directions request failed due to ' + status);
            }
          });
        },
        polyline:function(coords){
          // if (marker != null) {
          //   marker.setMap(null);
          // }
          if (line != null) {
            line.setMap(null);
          }
          var lineSymbol = {
            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
          };
          var line = new google.maps.Polyline({
            path: coords,
            fillOpacity: 0.8,
            fillColor:'#FF0000',
              scale: 6,
            strokeColor: '#FF0000',
             strokeOpacity: 0.8,
            strokeWeight: 4,
            icons: [{
              icon: lineSymbol,
              offset: '100%'
            }],
            map: map
          });
        },
        address_convetor:function(coords){
         var geocoder = new google.maps.Geocoder;

          geocoder.geocode({'location': cords}, function(results, status) {
            if (status === 'OK') {
              if (results[0]) {
               return results[0].formatted_address;
             }}
           })
        }
      }
    })



.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});
