
var app_id='WO_CFoA_O1tb23feBFTsK8u-r';
var model={};
var device_token='';
var backbutton=0;
var set={};
var get_token='';
var push='';
var interval='';
var xpay_id ='';
var curency = 'Rs.';
var token = 123456;
// var token_new = 123456;
var app_user = '';
var app_user = JSON.parse(localStorage.getItem('food_app_user'));

// var url = 'http://food4u/food_api/index.php';
// var url_new = 'http://food4u/api/';
// var img_url = 'http://food4u';

var url = 'http://food4u.acecommunicate.com/food_api/index.php';
var url_new = 'http://food4u.acecommunicate.com/api/';
var img_url = 'http://food4u.acecommunicate.com';

/*var url = 'http://food4u.lk/water_api/index.php';
var url_new = 'http://food4u.lk/api/';*/


angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','angular.filter', 'star-rating','tabSlideBox','ionMDRipple', 'ionic-pullup','ionic-SuperActionSheet','gm','ngCordova'])

.run(function($ionicPlatform,$rootScope,  $ionicLoading,$ionicLoading, $ionicPopup, $ionicHistory) {
  $ionicPlatform.ready(function() {

    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      if (ionic.Platform.isAndroid()) {
       StatusBar.backgroundColorByHexString('#f15522');
   } else {
       StatusBar.styleLightContent();
   }
    }

    ionic.Platform.fullScreen();

	      $rootScope.$on('loading:show', function() {
        $ionicLoading.show({  template: '<ion-spinner icon="ripple" class="spinner-balanced"></ion-spinner>',
            duration: 2000})
      })

      $rootScope.$on('loading:hide', function() {
        $ionicLoading.hide()
      })

      if(window.Connection) {
      if(navigator.connection.type == Connection.NONE) {
         document.addEventListener("offline", function () {
      $ionicPopup.show({
      title: 'Internet Disconnected',
      content: 'The internet is disconnected on your device.',
      buttons : [{
       text : 'Cancel',
       type : 'button-assertive button-outline',
      }, {
       text : 'Exit',
       type : 'button-assertive',
       onTap : function() {
         window.close();
         ionic.Platform.exitApp();
       }
      }]
      })
      .then(function(result) {
      if(!result) {
      window.close();
      ionic.Platform.exitApp();
      }
      });
      }, false);
      }
      }
  });
 $ionicPlatform.registerBackButtonAction(function(e) {
     e.preventDefault();
     function showConfirm() {
      var confirmPopup = $ionicPopup.show({
       title : 'Exit Food4U?',
       template : 'Are you sure you want to exit Food4u?',
       buttons : [{
        text : 'Cancel',
        type : 'button-assertive button-outline',
      }, {
        text : 'Ok',
        type : 'button-assertive',
        onTap : function() {

          ionic.Platform.exitApp();
        }
      }]
    });
    };
    if ($ionicHistory.backView()) {
      $ionicHistory.backView().go();
    } else {
      showConfirm();
    }
    return false;
  }, 101);
})
.directive('hideTabs', function($rootScope) {
    return {
        restrict: 'A',
        link: function(scope, element, attributes) {
			    scope.$watch(attributes.hideTabs, function(value){
            $rootScope.hideTabs = value;
			  });
            scope.$on('$destroy', function() {
                $rootScope.hideTabs = false;
            });


        }
    };
})
.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider,$httpProvider) {

	  $httpProvider.interceptors.push(function($rootScope) {
     return {
       request: function(config) {
         $rootScope.$broadcast('loading:show')
         return config
       },
       response: function(response) {
         $rootScope.$broadcast('loading:hide')
         return response
       }
     }
   })

	  $ionicConfigProvider.views.maxCache(0);
  $ionicConfigProvider.tabs.position('bottom');
	$ionicConfigProvider.views.transition('android');
	$ionicConfigProvider.backButton.text('').icon('ion-arrow-left-c').previousTitleText(false);
		$ionicConfigProvider.navBar.alignTitle('center');
	  $ionicConfigProvider.scrolling.jsScrolling(false);
	$ionicConfigProvider.views.swipeBackEnabled(true);
$ionicConfigProvider.views.swipeBackHitWidth(60);

  $stateProvider

    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    controller: 'AppCtrl'
  })
   .state('login', {
    url: '/login',
    cache:false,
    templateUrl: 'templates/login.html',
	  controller: 'LoginCtrl'
  })
   .state('register', {
    url: '/register',
    cache:false,
    templateUrl: 'templates/register.html',
	  controller: 'RegisterCtrl'
  })
.state('restaurants_search', {
    url: '/restaurants_search',
    cache:false,
     templateUrl: 'templates/tab-restaurants-search.html',
        controller: 'RestaurantsSearchCtrl'
  })
  // Each tab has its own nav history stack:
  .state('tab.restaurants-search', {
    url: '/restaurants-search',
    cache:false,
    views: {
      'tab-restaurants': {
        templateUrl: 'templates/tab-restaurants-search.html',
        controller: 'RestaurantsSearchCtrl'
      }
    }
  })
	  .state('tab.restaurants', {
    url: '/restaurants',
    params:{cords:null},
    cache:false,
    views: {
      'tab-restaurants': {
        templateUrl: 'templates/tab-restaurants.html',
        controller: 'RestaurantsCtrl'
      }
    }
  })
 .state('tab.restaurant-detail', {
    url: '/restaurants/:restaurantId',
    cache:false,
    views: {
      'tab-restaurants': {
        templateUrl: 'templates/restaurant-detail.html',
        controller: 'RestaurantDetailCtrl'
      }
    }
  })
	  .state('tab.cart', {
    url: '/cart',
    cache:false,
    views: {
      'tab-notification': {
        templateUrl: 'templates/cart.html',
        controller: 'CartCtrl'
      }
    }
  })
  .state('tab.checkout', {
url: '/checkout',
    params:{data:null},
cache:false,
views: {
  'tab-restaurants': {
    templateUrl: 'templates/checkout.html',
    controller: 'CheckoutCtrl'
  }
}

})

		  .state('tab.payment', {
    url: '/payment',
    params:{data:null},
    cache:false,
    views: {
      'tab-notification': {
        templateUrl: 'templates/payment.html',
        controller: 'PaymentCtrl'
      }
    }
  })

			  .state('tab.confirmation', {
    url: '/confirmation',
    cache:false,
    views: {
      'tab-restaurants': {
        templateUrl: 'templates/order-confirmation.html',
        controller: 'ConfirmationCtrl'
      }
    }
  })

  .state('tab.order', {
    url: '/order',
    cache:false,
    views: {
      'tab-order': {
        templateUrl: 'templates/tab-order.html',
        controller: 'OrderCtrl'
      }
    }
  })

	  .state('tab.notification', {
    url: '/notification',
    cache:false,
    views: {
      'tab-notification': {
        templateUrl: 'templates/tab-notification.html',
        controller: 'NotificationCtrl'
      }
    }
  })


//  .state('tab.chats', {
//      url: '/chats',
//      views: {
//        'tab-chats': {
//          templateUrl: 'templates/tab-chats.html',
//          controller: 'ChatsCtrl'
//        }
//      }
//    })
//    .state('tab.chat-detail', {
//      url: '/chats/:chatId',
//      views: {
//        'tab-chats': {
//          templateUrl: 'templates/chat-detail.html',
//          controller: 'ChatDetailCtrl'
//        }
//      }
//    })

  .state('tab.account', {
    url: '/account',
    cache:false,
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })
	  .state('tab.settings', {
    url: '/settings',
    cache:false,
    views: {
      'tab-account': {
        templateUrl: 'templates/settings.html',
        controller: 'SettingsCtrl'
      }
    }
  })
	   .state('tab.profile', {
    url: '/profile',
    cache:false,
    views: {
      'tab-account': {
        templateUrl: 'templates/profile.html',
        controller: 'ProfileCtrl'
      }
    }
  });

  if(localStorage.getItem('location'))
  {
      $urlRouterProvider.otherwise('/tab/restaurants');
  }
  else
  {
  $urlRouterProvider.otherwise('/restaurants_search');
}
// if(localStorage.getItem('food_app_user') != null)
//   {
//     data = JSON.parse(localStorage.getItem('food_app_user'));

//     if(data.role == 'DeliveryBoy')
//     {
//       $urlRouterProvider.otherwise('/app/delivery_order_list');
//     }
//     else
//     {
//       $urlRouterProvider.otherwise('/tab/restaurants');
//     }

//   }
//   else
//   {
//     $urlRouterProvider.otherwise('/tab/restaurants');
//   }
});
